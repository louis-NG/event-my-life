﻿using EventMyLife.Log;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Messaging;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using System.Windows.Forms;
using Timer = System.Timers.Timer;

namespace EventMyLife
{
    public partial class EventMyLife : ServiceBase
    {
        public EventMyLife()
        {
            InitializeComponent();
        }

        private Timer timer = null;
        protected override void OnStart(string[] args)
        {
            // Tạo 1 timer từ libary System.Timers
            timer = new Timer();
            // Execute mỗi 60s
            timer.Interval = 60000;
            // Những gì xảy ra khi timer đó dc tick
            timer.Elapsed += timer_Tick;
            // Enable timer
            timer.Enabled = true;
            // Ghi vào log file khi services dc start lần đầu tiên
            Utilities.WriteLogError("Test for 1st run WindowsService");
        }

        private void timer_Tick(object sender, ElapsedEventArgs args)
        {
            MessageBox.Show("This is my event", "Hello Louis", MessageBoxButtons.OKCancel);
            Utilities.WriteLogError("Timer has ticked for doing something!!!");
        }

        protected override void OnStop()
        {
            // Ghi log lại khi Services đã được stop
            timer.Enabled = true;
            Utilities.WriteLogError("1st WindowsService has been stop");
        }


    }
}

